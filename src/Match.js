import React, {Component} from 'react';
import './match.css';
import {Team} from "./Team/Team";
import {Scorers} from "./Team/Scorers";

class Match extends Component {

  liverpool = [
    'Van Beton',
    'Tomek',
    'Piotek',
    'Przemek',
  ];

   manchester = [
    'Mateo Carasco',
    'Mikerl Vaniardo',
    'Pablo Goloncardo',
    'Przemek',
  ];

   state = {
     team1: 0,
     team2: 0,
     scorers: []
   };

   handleClick = team => player => {

     this.setState({
       [team]: this.state[team] + 1,
       scorers: this.state.scorers.concat(player)
     });

   };

  render() {



    return (
      <div className="match">
        <header>Venue: {this.props.venue}</header>

        <Team name={'Liverpool Bryjów'} players={this.liverpool} score={this.state.team1} onClick={this.handleClick('team1')} />
        <Team name={'Manchester Lipnica'} players={this.manchester} score={this.state.team2} onClick={this.handleClick('team2')}  />

        <Scorers scorers={this.state.scorers} />
      </div>
    );
  }
}

export default Match;