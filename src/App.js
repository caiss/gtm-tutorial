import React from 'react';
import './App.css';
import Match from "./Match";

function App() {
  return (
    <div className="App">
      <Match venue="Orlik w Bryjowie" />
    </div>
  );
}

export default App;
