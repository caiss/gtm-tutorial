import React from "react";

export function Scorers(props) {

  const scorers = props.scorers.map(i => <li className="scorer">{i}</li>);

  return <div className="scorers">
    <h2>Scorers</h2>
    <ul>
      {scorers}
    </ul>


  </div>;
}