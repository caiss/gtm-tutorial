import React from "react";

export const Player = (props) => <div className="player" onClick={() => props.onClick(props.name)}>{props.name}</div>;