import React, {Component} from "react";
import {Player} from "./Player";

export class Team extends Component {
  render() {

    const players = this.props.players.map(i => <Player key={i} name={i} onClick={this.props.onClick}  />);

    return <div className="team">
      <h1 className={"teamName"}>{this.props.name}</h1>
      <h2 className={"teamscorw"}>{this.props.score}</h2>

      <div className="players">
        {players}
      </div>

    </div>;
  }
}